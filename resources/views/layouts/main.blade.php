<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- start linking  -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="/Admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="/Admin/css/app.css">
    <!-- icon -->
    <link rel="icon" href="/Admin/img/log.png">
    <!-- end linking -->
    <title>Travel Admin Template</title>
</head>
<body>
    <section id="admin">
  <!-- start sidebar -->
  <div class="sidebar">
    <!-- start with head -->
    <div class="head">
      <div class="logo">
        <img src="/Admin/img/logo-admin.png" alt="">
      </div>
      <a href="#" class="btn btn-danger">Travel Admin Panel</a>
    </div>
    <!-- end with head -->
    <!-- start the list -->
    <div id="list">
      <ul class="nav flex-column">
        <li class="nav-item"><a href="/home" class="nav-link active" ><i class="fa fa-adjust"></i>Dashboard</a></li>
        <li class="nav-item"><a href="/contacts" class="nav-link"><i class="fa fa-user"></i>Contacts</a></li>
        <!-- end user interface submenu -->
    </div>
    <!-- end the list -->
  </div>
  <!-- end sidebar -->
  @yield('content')

  <!-- start screpting -->
<script src="/Admin/js/jquery.min.js"></script>
<script src="/Admin/js/tether.min.js"></script>
<script src="/Admin/js/bootstrap.min.js"></script>
<script src="/Admin/js/highcharts.js"></script>
<script src="/Admin/js/chart.js"></script>
<script src="/Admin/js/app.js"></script>
<!-- end screpting -->
</body>
</html>